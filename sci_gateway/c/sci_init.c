/*
    This file is part of Epfs.

    Epfs is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    Epfs is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with Epfs. If not, see <http://www.gnu.org/licenses/>.

    Copyright (c) 2010, Baozeng Ding.
*/
#include <Python.h>
#include "api_scilab.h"
#include "stack-c.h"

int sci_init(char *fname)
{
	CheckRhs(0, 0);

	py_initialize();

	LhsVar(1) = 0;
	return 0;
}
