"""
    This file is part of Epfs.

    Epfs is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    Epfs is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with Epfs. If not, see <http://www.gnu.org/licenses/>.

    Copyright (c) 2010, Baozeng Ding.
"""

'''py_class.py - Python class source designed to demonstrate the use of embedding python from scilab'''

class Multiply:
	def __init__(self):
		self.a = 6
		self.b = 8

	def multiply(self):
		c = self.a * self.b
		print 'The result of', self.a, 'x', self.b, ':', c
		return c

	def multiply2(self, a, b):
		c = a * b
		print 'The result of', a, 'x', b, ':', c
		return c
	def F(self, a):
		return a
